/*
*
*Author: Pavithra Gajendra
*Description: Utility Class
*Created Date: 27/04/16
*Version: 1.0
*
*/ 
public class UtilityMethods {
    
    /***********************************************************
    Purpose     : Method for getting Record Type Id , 
                   with parameters as Object & record type Name                                  
    Return Type : Record Id
    *************************************************************/
    public static Id getRecordTypeId(String objectType,String recordTypeName){
                
        Id recTypeId = Schema.getGlobalDescribe().get(objectType).getDescribe().getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        return recTypeId ;      
    }
    
     /****************************************************
    Purpose     : Get List of String in Lower Case            
    Return Type : String
    *****************************************************/
    public static Set<String> getStringInLoweCase(String stringList){
        
        Set<String> returnList = new Set<String>();
        for(String s : stringList.split(',')){
            returnList.add(s.toLowerCase());
        }        
        return returnList;
    } 
       
   /*****************************************
    Purpose     : Method for Sending Email                                  
    Return Type : Nothing
    *****************************************/
    public static void sendEmailMethod(List<String> emailList,String emailBody,String subject){
        
        Messaging.SingleEmailMessage mailHandler = new Messaging.SingleEmailMessage();
        mailHandler.setToAddresses(emailList);
        mailHandler.setSenderDisplayName(userInfo.getName());
        mailHandler.setSubject(subject);
        mailHandler.setHtmlBody(emailBody);
        
        try{
            Messaging.sendEmail(new Messaging.Email[] { mailHandler });
        }catch(EmailException ex){
            System.debug(ex.getMessage());
        }
    }
    
    /*****************************************
    Purpose     : Method to get profileId                                  
    Return Type : Record Id
    *****************************************/
    public static Id getProfileId(String profileName){
        
        return [Select Id FROM Profile Where Name=:profileName].Id ;
    }
    
    /************************************************************
    Purpose     : Method to get all picklist values for a field
    Return Type : String list
    *************************************************************/
    public static List<String> getAllPicklistValues(String objectType,String fieldName){
        List<String> allPicklistValue = new List<String>();
        
        return allPicklistValue ;
    }
    
     /************************************************************
    Purpose     : Method to get key prefix of a object
    Return Type : String 
    *************************************************************/
    public static String getKeyPrefixOfSobject(Sobject objectName){
            String keyPrefix = 'Schema.getGlobalDescribe().get(objectName).getDescribe().getKeyPrefix()';
            return keyPrefix;
        }
    
}